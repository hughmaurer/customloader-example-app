//
//  PlainLoader.h
//  LoaderObjC
//
//  Created by Maurer on 05/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "Loader.h"

@interface PlainLoader : Loader

/*!
 *  Generates the shape path of the plain loader
 *
 *  @return The shape path
 */
- (CGPathRef) shapePath;

@end
