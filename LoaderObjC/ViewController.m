//
//  ViewController.m
//  LoaderObjC
//
//  Created by Maurer on 05/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property UIButton *button;
@property PlainLoader *loader;
@property (weak, nonatomic) IBOutlet UILabel *percentlabel;
@property (weak, nonatomic) IBOutlet UIButton *switchButton;

@end

@implementation ViewController

- (CGPathRef) testpath{
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(31.9, 83.6)];
    [bezierPath addLineToPoint: CGPointMake(13.7, 127.7)];
    [bezierPath addLineToPoint: CGPointMake(153.7, 127.7)];
    [bezierPath addLineToPoint: CGPointMake(133.4, 77.9)];
    [bezierPath addLineToPoint: CGPointMake(133.2, 77.4)];
    [bezierPath addLineToPoint: CGPointMake(101.5, -0.2)];
    [bezierPath addLineToPoint: CGPointMake(71.3, 73.7)];
    [bezierPath addLineToPoint: CGPointMake(50.4, 38.8)];
    [bezierPath addLineToPoint: CGPointMake(31.9, 83.6)];
    [bezierPath closePath];
    bezierPath.miterLimit = 4;
    bezierPath.lineWidth = 7;
    
    return  bezierPath.CGPath;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.button = [[UIButton alloc] init];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // All possible Loaders
    
    //_loader = [PlainLoader showLoaderWithPath: [self testpath]];
    //_loader = [PlainLoader showProgressBasedLoaderWithPath:[self testpath]];
    _loader = [SpikeLoader showLoaderWithPath: [self testpath]];
    //_loader = [SpikeLoader showProgressBasedLoaderWithPath:[self testpath]];
    //_loader = [RoundedLoader showLoaderWithPath: [self testpath]];
    //_loader = [RoundedLoader showProgressBasedLoaderWithPath:[self testpath]];
    //_loader = [WavesLoader showLoaderWithPath: [self testpath]];
    //_loader = [WavesLoader showProgressBasedLoaderWithPath:[self testpath]];
    //_loader = [MovingSpikesLoader showLoaderWithPath: [self testpath]];
    //_loader = [MovingSpikesLoader showProgressBasedLoaderWithPath:[self testpath]];
    
    [self setUpSubviews];
}

- (void) setUpSubviews
{
    
    UIImage *image = [UIImage imageNamed:@"bg.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = [[self view] frame];
    [self.view addSubview: imageView];
    
    CGFloat margin = 30;
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    if (_loader.progressBased)
        [_percentlabel setText:[NSString stringWithFormat:@"%f\%", _loader.progress]];
    else
    {
        self.button.hidden = true;
        [_percentlabel setText:[NSString stringWithFormat:@"Not in progress based mode"]];
    }
    
    self.button.frame = CGRectMake(0, 0, 150, 35);
    self.button.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 2*margin - 40);
    self.button.backgroundColor = [UIColor redColor];
    self.button.layer.cornerRadius = 5.0;
    [self.button setTitle:@"+5" forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(add5progress) forControlEvents:UIControlEventTouchUpInside];
    [window addSubview:self.button];
    [window bringSubviewToFront:self.button];
}

- (void)add5progress
{
    if (_loader.progressBased)
    {
        if (_loader.progress + 0.05 < 1.0)
            _loader.progress = _loader.progress + 0.05;
        else
            _loader.progress = _loader.progress - 0.95;
        [_percentlabel setText:[NSString stringWithFormat:@"%f\%", _loader.progress]];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

@end
