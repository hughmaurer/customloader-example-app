//
//  WavesLoader.h
//  LoaderObjC
//
//  Created by Maurer on 06/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "Loader.h"

@interface WavesLoader : Loader

/*!
 *  Starts the animation of the waves.
 */
- (void) startWaving;

/*!
 *  Returns an array containing the waves's shapes.
 *
 *  @param count Count of waves
 *
 *  @return Array of shapes
 */
- (NSMutableArray*) shapesArray:(int) count;

/*!
 *  Generates the shape path of waves, for creating the waves's shapes array.
 *
 *  @param index Index helps to know if it is the beginning or the end
 *  @param count Count of waves
 *
 *  @return Path of the waves shapes
 */
- (CGPathRef) shapePathAtIndex:(int) index andCount:(int) count;

@end
