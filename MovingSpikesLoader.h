//
//  TestLoader.h
//  LoaderObjC
//
//  Created by Maurer on 06/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "Loader.h"

@interface MovingSpikesLoader : Loader

/*!
 *  The variation of the spikes height
 */
@property CGFloat variation;

/*!
 *  Boolean value to know if the variation must be incremented or decremented.
 */
@property bool incr;

/*!
 *  Starts the animation of the spikes moving up and down
 */
- (void) startWaving;


/*!
 *  Returns an array containing the spikes's shapes.
 *
 *  @param count Count of waves
 *
 *  @return Array of shapes
 */
- (NSMutableArray*) shapesArray:(int) count;

/*!
 *  Generates the shape path of spikes, for creating the spikes's shapes array.
 *
 *  @param index Index helps to know if it is the beginning or the end
 *  @param count Count of spikes
 *
 *  @return Path of the spikes shapes
 */
- (CGPathRef) shapePathAtIndex:(int) index andCount:(int) count;

@end
