//
//  RoundedLoader.h
//  LoaderObjC
//
//  Created by Maurer on 06/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "Loader.h"

@interface RoundedLoader : Loader

/*!
 *  Spikes heaight
 */
@property CGFloat spikeHeight;

/*!
 *  Generates the shape path of the rounded loader
 *
 *  @return The shape path
 */
- (CGPathRef) shapePath;

@end
