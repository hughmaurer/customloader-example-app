//
//  PlainLoader.m
//  LoaderObjC
//
//  Created by Maurer on 05/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "PlainLoader.h"

@implementation PlainLoader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (CGPathRef) shapePath
{
    CGFloat w_shapepath = self.loaderView.frame.size.width;
    CGFloat h_shapepath = self.loaderView.frame.size.height;
    
    UIBezierPath* bezierPath2 = [UIBezierPath bezierPath];
    [bezierPath2 moveToPoint: CGPointMake(0, h_shapepath / 2)];
    
    [bezierPath2 addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath / 2)];
    [bezierPath2 addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath * 2)];
    [bezierPath2 addLineToPoint: CGPointMake(0, h_shapepath * 2)];
    [bezierPath2 closePath];
    bezierPath2.miterLimit = 4;
    bezierPath2.lineWidth = 7;
    return bezierPath2.CGPath;
}

+ (instancetype)createLoaderWithPath:(CGPathRef)path
{
    PlainLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    [loader addPath:path];
    return loader;
}

+ (instancetype)showLoaderWithPath:(CGPathRef)path
{
    PlainLoader *loader = [PlainLoader createLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

+ (instancetype)createProgressBasedLoaderWithPath:(CGPathRef)path
{
    PlainLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    loader.progressBased = true;
    [loader addPath:path];
    return loader;
}

+ (instancetype)showProgressBasedLoaderWithPath:(CGPathRef)path
{
    PlainLoader *loader = [PlainLoader createProgressBasedLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

- (void)generateLoader
{
    [self layoutPath];
}

- (void)startAnimating
{
    if (!self.animate)
    {
        return;
    }
    // [self startSwinging];
    [self startMoving: true];
}

- (void)layoutPath
{
    [super layoutPath];
    self.shapeLayer.path = [self shapePath];
}

@end
