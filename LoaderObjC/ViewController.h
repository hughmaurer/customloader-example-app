//
//  ViewController.h
//  LoaderObjC
//
//  Created by Maurer on 05/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Loader.h"
#import "PlainLoader.h"
#import "SpikeLoader.h"
#import "RoundedLoader.h"
#import "WavesLoader.h"
#import "MovingSpikesLoader.h"

@interface ViewController : UIViewController

@end

